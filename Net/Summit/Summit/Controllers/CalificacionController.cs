﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Summit.Models;

namespace Summit.Controllers
{
    public class CalificacionController : Controller
    {
        private SummitEntities1 db = new SummitEntities1();

        // GET: Calificacion
        public ActionResult Index()
        {
            var calificacion = db.Calificacion.Include(c => c.Evento).Include(c => c.Usuario);
            return View(calificacion.ToList());
        }

        // GET: Calificacion/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Calificacion calificacion = db.Calificacion.Find(id);
            if (calificacion == null)
            {
                return HttpNotFound();
            }
            return View(calificacion);
        }

        // GET: Calificacion/Create
        public ActionResult Create()
        {
            ViewBag.IDEvento = new SelectList(db.Evento, "IDEvento", "IDEvento");
            ViewBag.IDUsuario = new SelectList(db.Usuario, "IDUsuario", "Nombre");
            return View();
        }

        // POST: Calificacion/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IDCalifiacion,IDEvento,IDUsuario,Calificacion1,Comentario")] Calificacion calificacion)
        {
            if (ModelState.IsValid)
            {
                db.Calificacion.Add(calificacion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IDEvento = new SelectList(db.Evento, "IDEvento", "IDEvento", calificacion.IDEvento);
            ViewBag.IDUsuario = new SelectList(db.Usuario, "IDUsuario", "Nombre", calificacion.IDUsuario);
            return View(calificacion);
        }

        // GET: Calificacion/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Calificacion calificacion = db.Calificacion.Find(id);
            if (calificacion == null)
            {
                return HttpNotFound();
            }
            ViewBag.IDEvento = new SelectList(db.Evento, "IDEvento", "IDEvento", calificacion.IDEvento);
            ViewBag.IDUsuario = new SelectList(db.Usuario, "IDUsuario", "Nombre", calificacion.IDUsuario);
            return View(calificacion);
        }

        // POST: Calificacion/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IDCalifiacion,IDEvento,IDUsuario,Calificacion1,Comentario")] Calificacion calificacion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(calificacion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IDEvento = new SelectList(db.Evento, "IDEvento", "IDEvento", calificacion.IDEvento);
            ViewBag.IDUsuario = new SelectList(db.Usuario, "IDUsuario", "Nombre", calificacion.IDUsuario);
            return View(calificacion);
        }

        // GET: Calificacion/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Calificacion calificacion = db.Calificacion.Find(id);
            if (calificacion == null)
            {
                return HttpNotFound();
            }
            return View(calificacion);
        }

        // POST: Calificacion/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Calificacion calificacion = db.Calificacion.Find(id);
            db.Calificacion.Remove(calificacion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
