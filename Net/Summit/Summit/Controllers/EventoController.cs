﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Summit.Models;

namespace Summit.Controllers
{
    public class EventoController : Controller
    {
        private SummitEntities1 db = new SummitEntities1();

        // GET: Evento
        public ActionResult Index()
        {
            var evento = db.Evento.Include(e => e.Tema).Include(e => e.TipoEvento).Include(e => e.Ubicacion).Include(e => e.Usuario);
            return View(evento.ToList());
        }

        // GET: Evento/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Evento evento = db.Evento.Find(id);
            if (evento == null)
            {
                return HttpNotFound();
            }
            return View(evento);
        }

        // GET: Evento/Create
        public ActionResult Create()
        {
            ViewBag.IDTema = new SelectList(db.Tema, "IDTema", "Descripcion");
            ViewBag.IDTipoEvento = new SelectList(db.TipoEvento, "IDTipoEvento", "Descripcion");
            ViewBag.IDUbicacion = new SelectList(db.Ubicacion, "IDUbicacion", "Lugar");
            ViewBag.IDExpositor = new SelectList(db.Usuario, "IDUsuario", "Nombre");
            return View();
        }

        // POST: Evento/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IDEvento,IDTipoEvento,FechaInicio,FechaFinal,IDExpositor,IDTema,Limite,Estado,Calificacion,IDUbicacion")] Evento evento)
        {
            if (ModelState.IsValid)
            {
                db.Evento.Add(evento);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IDTema = new SelectList(db.Tema, "IDTema", "Descripcion", evento.IDTema);
            ViewBag.IDTipoEvento = new SelectList(db.TipoEvento, "IDTipoEvento", "Descripcion", evento.IDTipoEvento);
            ViewBag.IDUbicacion = new SelectList(db.Ubicacion, "IDUbicacion", "Lugar", evento.IDUbicacion);
            ViewBag.IDExpositor = new SelectList(db.Usuario, "IDUsuario", "Nombre", evento.IDExpositor);
            return View(evento);
        }

        // GET: Evento/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Evento evento = db.Evento.Find(id);
            if (evento == null)
            {
                return HttpNotFound();
            }
            ViewBag.IDTema = new SelectList(db.Tema, "IDTema", "Descripcion", evento.IDTema);
            ViewBag.IDTipoEvento = new SelectList(db.TipoEvento, "IDTipoEvento", "Descripcion", evento.IDTipoEvento);
            ViewBag.IDUbicacion = new SelectList(db.Ubicacion, "IDUbicacion", "Lugar", evento.IDUbicacion);
            ViewBag.IDExpositor = new SelectList(db.Usuario, "IDUsuario", "Nombre", evento.IDExpositor);
            return View(evento);
        }

        // POST: Evento/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IDEvento,IDTipoEvento,FechaInicio,FechaFinal,IDExpositor,IDTema,Limite,Estado,Calificacion,IDUbicacion")] Evento evento)
        {
            if (ModelState.IsValid)
            {
                db.Entry(evento).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IDTema = new SelectList(db.Tema, "IDTema", "Descripcion", evento.IDTema);
            ViewBag.IDTipoEvento = new SelectList(db.TipoEvento, "IDTipoEvento", "Descripcion", evento.IDTipoEvento);
            ViewBag.IDUbicacion = new SelectList(db.Ubicacion, "IDUbicacion", "Lugar", evento.IDUbicacion);
            ViewBag.IDExpositor = new SelectList(db.Usuario, "IDUsuario", "Nombre", evento.IDExpositor);
            return View(evento);
        }

        // GET: Evento/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Evento evento = db.Evento.Find(id);
            if (evento == null)
            {
                return HttpNotFound();
            }
            return View(evento);
        }

        // POST: Evento/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Evento evento = db.Evento.Find(id);
            db.Evento.Remove(evento);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
