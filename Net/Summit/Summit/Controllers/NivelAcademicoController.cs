﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Summit.Models;

namespace Summit.Controllers
{
    public class NivelAcademicoController : Controller
    {
        private SummitEntities1 db = new SummitEntities1();

        // GET: NivelAcademico
        public ActionResult Index()
        {
            return View(db.NivelAcademico.ToList());
        }

        // GET: NivelAcademico/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NivelAcademico nivelAcademico = db.NivelAcademico.Find(id);
            if (nivelAcademico == null)
            {
                return HttpNotFound();
            }
            return View(nivelAcademico);
        }

        // GET: NivelAcademico/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: NivelAcademico/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IDNivel,Descripcion")] NivelAcademico nivelAcademico)
        {
            if (ModelState.IsValid)
            {
                db.NivelAcademico.Add(nivelAcademico);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(nivelAcademico);
        }

        // GET: NivelAcademico/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NivelAcademico nivelAcademico = db.NivelAcademico.Find(id);
            if (nivelAcademico == null)
            {
                return HttpNotFound();
            }
            return View(nivelAcademico);
        }

        // POST: NivelAcademico/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IDNivel,Descripcion")] NivelAcademico nivelAcademico)
        {
            if (ModelState.IsValid)
            {
                db.Entry(nivelAcademico).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(nivelAcademico);
        }

        // GET: NivelAcademico/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NivelAcademico nivelAcademico = db.NivelAcademico.Find(id);
            if (nivelAcademico == null)
            {
                return HttpNotFound();
            }
            return View(nivelAcademico);
        }

        // POST: NivelAcademico/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            NivelAcademico nivelAcademico = db.NivelAcademico.Find(id);
            db.NivelAcademico.Remove(nivelAcademico);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
