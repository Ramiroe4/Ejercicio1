﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Summit.Models;

namespace Summit.Controllers
{
    public class UbicacionController : Controller
    {
        private SummitEntities1 db = new SummitEntities1();

        // GET: Ubicacion
        public ActionResult Index()
        {
            var ubicacion = db.Ubicacion.Include(u => u.Canton).Include(u => u.Distrito).Include(u => u.Provincia);
            return View(ubicacion.ToList());
        }

        // GET: Ubicacion/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ubicacion ubicacion = db.Ubicacion.Find(id);
            if (ubicacion == null)
            {
                return HttpNotFound();
            }
            return View(ubicacion);
        }

        // GET: Ubicacion/Create
        public ActionResult Create()
        {
            ViewBag.IDCanton = new SelectList(db.Canton, "IDCanton", "Nombre");
            ViewBag.IDDistrito = new SelectList(db.Distrito, "IDdistrito", "Nombre");
            ViewBag.IDProvincia = new SelectList(db.Provincia, "IDProvincia", "Nombre");
            return View();
        }

        // POST: Ubicacion/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IDUbicacion,IDProvincia,IDCanton,IDDistrito,Lugar")] Ubicacion ubicacion)
        {
            if (ModelState.IsValid)
            {
                db.Ubicacion.Add(ubicacion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IDCanton = new SelectList(db.Canton, "IDCanton", "Nombre", ubicacion.IDCanton);
            ViewBag.IDDistrito = new SelectList(db.Distrito, "IDdistrito", "Nombre", ubicacion.IDDistrito);
            ViewBag.IDProvincia = new SelectList(db.Provincia, "IDProvincia", "Nombre", ubicacion.IDProvincia);
            return View(ubicacion);
        }

        // GET: Ubicacion/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ubicacion ubicacion = db.Ubicacion.Find(id);
            if (ubicacion == null)
            {
                return HttpNotFound();
            }
            ViewBag.IDCanton = new SelectList(db.Canton, "IDCanton", "Nombre", ubicacion.IDCanton);
            ViewBag.IDDistrito = new SelectList(db.Distrito, "IDdistrito", "Nombre", ubicacion.IDDistrito);
            ViewBag.IDProvincia = new SelectList(db.Provincia, "IDProvincia", "Nombre", ubicacion.IDProvincia);
            return View(ubicacion);
        }

        // POST: Ubicacion/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IDUbicacion,IDProvincia,IDCanton,IDDistrito,Lugar")] Ubicacion ubicacion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ubicacion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IDCanton = new SelectList(db.Canton, "IDCanton", "Nombre", ubicacion.IDCanton);
            ViewBag.IDDistrito = new SelectList(db.Distrito, "IDdistrito", "Nombre", ubicacion.IDDistrito);
            ViewBag.IDProvincia = new SelectList(db.Provincia, "IDProvincia", "Nombre", ubicacion.IDProvincia);
            return View(ubicacion);
        }

        // GET: Ubicacion/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ubicacion ubicacion = db.Ubicacion.Find(id);
            if (ubicacion == null)
            {
                return HttpNotFound();
            }
            return View(ubicacion);
        }

        // POST: Ubicacion/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Ubicacion ubicacion = db.Ubicacion.Find(id);
            db.Ubicacion.Remove(ubicacion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
