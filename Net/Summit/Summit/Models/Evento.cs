//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Summit.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Evento
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Evento()
        {
            this.Calificacion1 = new HashSet<Calificacion>();
            this.Reserva = new HashSet<Reserva>();
        }
    
        public int IDEvento { get; set; }
        public Nullable<int> IDTipoEvento { get; set; }
        public Nullable<System.DateTime> FechaInicio { get; set; }
        public Nullable<System.DateTime> FechaFinal { get; set; }
        public Nullable<int> IDExpositor { get; set; }
        public Nullable<int> IDTema { get; set; }
        public Nullable<int> Limite { get; set; }
        public Nullable<int> Estado { get; set; }
        public Nullable<int> Calificacion { get; set; }
        public Nullable<int> IDUbicacion { get; set; }
        public string NombreEvento { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Calificacion> Calificacion1 { get; set; }
        public virtual Tema Tema { get; set; }
        public virtual TipoEvento TipoEvento { get; set; }
        public virtual Ubicacion Ubicacion { get; set; }
        public virtual Usuario Usuario { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Reserva> Reserva { get; set; }
    }
}
