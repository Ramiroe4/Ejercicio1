﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Summit.Startup))]
namespace Summit
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
